ackage cn.com.android123;

引入声明 
import cn.com.android123.IAtmService;

// 声明一个接口，这里演示的是银行ATM程序
interface IBankAccountService {
	int getAccountBalance(); //返回整数，无参数
	void setOwnerNames(in List<String> names); //不返回，包含一个传入List参数
	BankAccount createAccount(in String name, int startingDeposit, in IAtmService atmService); //返回一个自定义类型
	int getCustomerList(in String branch, out String[] customerList); //返回整形，输入一个分支，输出一个客户列表
}